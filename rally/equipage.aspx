﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="equipage.aspx.cs" Inherits="rally.equipage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1>Insertion équipage</h1>
    <form id="form1" runat="server">
    <div>
        <div ID="message" runat="server"></div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label" runat="server" Text="Pilote"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropDownListPilote" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Copilote"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropDownListCopilote" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnInsertEquipage" runat="server" Text="Valider" 
                        onclick="btnInsertEquipage_Click"/>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
