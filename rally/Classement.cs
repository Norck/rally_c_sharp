﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Classement:InfoES
    {
        private float vitesseMoyenne;
        private float point;
        private int rang;
        private DateTime penalite;
        private DateTime tempsAvecPenalite;
        
        private float sTemps;
        private float sPenalite;
        private float sTempsAvecPenalite;

        
        public Classement() { }
        public void calculVitesseMoyenne() { 
            Utility utility = new Utility();
            float seconds = utility.dateTimeToSeconds(this.Temps);
            float vitesseMS = this.Distance / seconds;
            float vitesseKM = (vitesseMS * 3600)/1000;
            this.VitesseMoyenne = vitesseKM;
        }
        /*public Classement[] setRang(Classement[] tab) { 
        
        }*/
        public Classement[] trieTemps2(Classement[] tab) { 
            Classement[] tabTrie = new Classement[tab.Length];
            Classement temp = new Classement();
            for (int i = 0; i < tab.Length; i++) {
                for (int j = i; j < tab.Length; j++) {
                    if (DateTime.Compare(tab[i].Temps, tab[j].Temps) > 0) {
                        temp = tab[i];
                        tab[i] = tab[j];
                        tab[j] = temp;
                    }
                }
                tabTrie[i] = tab[i];
            }
            return tabTrie;
        }

        public void setPenalite(InfoLiaison infoLiaison)
        {
            DateTime tempsMin = infoLiaison.TempsOptimal;
            DateTime tempsMax = tempsMin.AddMinutes(1);
            Utility utility = new Utility();
            DateTime temps = infoLiaison.Temps;
            DateTime tempsOptimal = infoLiaison.TempsOptimal;
            if (DateTime.Compare(temps, tempsMin) < 0)
            {
                int diffMinute = new Utility().getDiffMinute(temps, tempsOptimal);
                this.SPenalite += diffMinute * 60;
                this.STempsAvecPenalite += diffMinute * 60;
                this.tempsAvecPenalite = this.TempsAvecPenalite.AddSeconds(diffMinute * 60);
            }
            else if (DateTime.Compare(temps, tempsMin) > 0)
            {
                int diffMinute = new Utility().getDiffMinute(temps, tempsOptimal);
                this.SPenalite += diffMinute * 10;
                this.STempsAvecPenalite += diffMinute * 10;
                this.tempsAvecPenalite = this.TempsAvecPenalite.AddSeconds(diffMinute * 10);
            }
            
        }

        public Classement[] appliquerPenalite(Classement[] tab) {
            
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            NpgsqlConnection connection = null;
            try
            {
               connection = dbConnect.connect();
               object[] dbPenalite = sqlUtility.multiFind(new InfoLiaison(), "infoLiaison", "nomRally='" + this.NomRally + "'", connection);
               InfoLiaison[] tabInfoLiaison = new InfoLiaison[dbPenalite.Length];
               for (int i = 0; i < dbPenalite.Length; i++) {
                   tabInfoLiaison[i] = (InfoLiaison)dbPenalite[i];
               }
                for (int i = 0; i < tab.Length; i++) {
                    for (int u = 0; u < tabInfoLiaison.Length; u++) {
                        if (string.Equals(tabInfoLiaison[u].IdEquipage, tab[i].IdEquipage)) {
                            tab[i].setPenalite(tabInfoLiaison[u]);
                        }
                    }
                }
            }
            catch (Exception e) {
                throw e;
            }

            return tab;
        }
            
        public Classement(string nomRally, string idES, string numES,
            string idEquipage, DateTime temps, DateTime tempsIdeal,
            int distance, string sexe, string idCategorieVoiture) {
            this.NomRally = nomRally;
            this.IdES = idES;
            this.NumES = numES;
            this.IdEquipage = idEquipage;
            this.Temps = temps;
            this.TempsIdeal = tempsIdeal;
            this.Distance = distance;
            this.Sexe = sexe;
            this.IdCategorieVoiture = idCategorieVoiture;
            this.Penalite = new DateTime(2019, 12, 30, 0, 0, 0, 0);
            this.TempsAvecPenalite = this.Temps;
            calculVitesseMoyenne();

            Utility utility = new Utility();
            this.SPenalite = 9;
            this.sTemps = utility.dateTimeToSeconds2(this.Temps);
            this.sPenalite = utility.dateTimeToSeconds2(this.Penalite);
        }

        public Classement[] filtreCategorie(Classement[] tab, string categorieVoiture)
        {
            Classement[] tabClassement = new Classement[0];
            List<Classement> listClassement = new List<Classement>();
            for (int i = 0; i < tab.Length; i++) {
                if (string.Equals(categorieVoiture, tab[i].IdCategorieVoiture)) {
                    listClassement.Add(tab[i]);
                }
            }
            tabClassement = listClassement.ToArray();

            return tabClassement;
        }

        public void attribuerPoint()
        {
            if (this.Rang == 1) {
                point = 25;
            }
            else if (this.Rang == 2) {
                point = 18;
            }
            else if (this.Rang == 3)
            {
                point = 15;
            }
            else if (this.Rang == 4)
            {
                point = 12;
            }
            else if (this.Rang == 5)
            {
                point = 10;
            }
        }

        public float Point {
            get {
                return point;
            }
            set {
                point = value;
            }
        }
        public int Rang {
            get {
                return rang;
            }
            set {
                rang = value;
            }
        }
        public DateTime Penalite {
            get {
                return penalite;
            }
            set {
                penalite = value;
            }
        }
        public float VitesseMoyenne {
            get {
                return vitesseMoyenne;
            }
            set {
                vitesseMoyenne = value;
            }
        }

        public DateTime TempsAvecPenalite {
            get {
                return tempsAvecPenalite;
            }
            set {
                this.tempsAvecPenalite = value;
            }
        }

        public float STemps
        {
            get
            {
                return sTemps;
            }
            set
            {
                sTemps = value;
            }
        }
        public float SPenalite
        {
            get
            {
                return sPenalite;
            }
            set
            {
                sPenalite = value;
            }
        }
        public float STempsAvecPenalite
        {
            get
            {
                return sTempsAvecPenalite;
            }
            set
            {
                sTempsAvecPenalite = value;
            }
        }
    }
}