﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class TempsES
    {
        private string idES;
        private string idEquipage;
        private DateTime temps;
        private int confirmation;

        public TempsES(){}

        public TempsES(string idES, string idEquipage, DateTime temps, int confirmation){
            this.IdES = idES;
            this.IdEquipage = idEquipage;
            this.Temps = temps;
            this.Confirmation = confirmation;
        }

        public string IdES {
            get {
                return idES;
            }
            set {
                idES = value;
            }
        }
        public string IdEquipage {
            get {
                return idEquipage;
            }
            set {
                idEquipage = value;
            }
        }
        public DateTime Temps {
            get {
                return temps;
            }
            set {
                temps = value;
            }
        }
        public int Confirmation{
            get{
                return confirmation;
            }
            set{
                confirmation = value;
            }
        }
    }
}