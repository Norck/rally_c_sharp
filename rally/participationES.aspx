﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="participationES.aspx.cs" Inherits="rally.participationES" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1>Formulaire insertion participationES.</h1>
    <form id="form1" runat="server">
        <div>
            <div ID="message" runat="server"></div>
            <table>
                    <td><asp:Label ID="Label5" runat="server" Text="rally"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListRally" runat="server" 
                            onselectedindexchanged="dropDownListRally_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnGetListRally" runat="server" Text="getListRally" 
                            onclick="btnGetListRally_Click"/>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text="idES"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListES" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="dropDownListES_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server" Text="idEquipage"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListIdEquipage" runat="server" 
                            AutoPostBack="True">
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label3" runat="server" Text="idVoiture"></asp:Label></td>
                    <td><asp:TextBox ID="txtIdVoiture" runat="server" Text=""></asp:TextBox></td>
                </tr>
                <tr>
                <td></td>
                    <td>
                        <asp:Button ID="btnInsert" runat="server" Text="insert" 
                            onclick="btnInsert_Click"/>
                        <asp:Button ID="btnGet" runat="server" Text="get" onclick="btnGet_Click" />
                        <asp:Button ID="btnGetAll" runat="server" Text="getAll" 
                            onclick="btnGetAll_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" onclick="btnClear_Click1" 
                            style="height: 26px"/>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gvParticipationES" runat="server">
                <Columns>
                    <asp:BoundField DataField="idES" HeaderText="idES" />
                    <asp:BoundField DataField="idEquipage" HeaderText="idEquipage" />
                    <asp:BoundField DataField="idVoiture" HeaderText="idVoiture" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkParticipationES" runat="server"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
