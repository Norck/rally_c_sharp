﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Equipage
    {
        private string idEquipage;
        private string nomPilote;
        private string copilote;

        public Equipage() { }

        public Equipage(string idEquipage, string nomPilote, string copilote) {
            this.IdEquipage = idEquipage;
            this.NomPilote = nomPilote;
            this.Copilote = copilote;
        }
        public Equipage(string nomPilote, string copilote)
        {
            char[] charPilote = nomPilote.ToCharArray();
            char[] charCopilote = copilote.ToCharArray();
            charCopilote[0] = char.ToUpper(charCopilote[0]);
            Utility utility = new Utility();
            char[] charIdEquipage = utility.concat2CharArray(charPilote, charCopilote);

            this.IdEquipage = new string(charIdEquipage);
            this.NomPilote = nomPilote;
            this.Copilote = copilote;
        }

        public string IdEquipage {
            get {
                return idEquipage;
            }
            set {
                idEquipage = value;
            }
        }
        public string NomPilote {
            get {
                return nomPilote;
            }
            set {
                nomPilote = value;
            }
        }
        public string Copilote {
            get {
                return copilote;
            }
            set {
                copilote = value;
            }
        }
    }
}