﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Device.Location;

namespace rally
{
    public class SqlUtility
    {
        /*
        public object[] getGeom(object[] tab, string nomTable, string apsWhere, NpgsqlConnection con) {
            NpgsqlDataReader reader = null;
            NpgsqlCommand cmd = null;
            PropertyInfo[] properties = tab[0].GetType().GetProperties();
            List<PropertyInfo> propGeom = new List<PropertyInfo>();
            string query = "select ";
            for (int i = 0; i < properties.Length; i++) {
                if (string.Equals("geoDepart", properties[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    propGeom.Add(properties[i]);
                }
            }
            PropertyInfo[] tabPropGeom = propGeom.ToArray();
            for (int i = 0; i < tabPropGeom.Length; i++) {
                if (i == (tabPropGeom.Length - 1))
                {
                    query += " st_asgeojson(" + tabPropGeom[i].Name + ") ";
                }
                else {
                    query += "st_asgeojson(" + tabPropGeom[i].Name + "), ";
                }
            }
            query += "from " + nomTable;
            if (apsWhere != null){
                query += " where " + apsWhere;
            }
            try
            {
                cmd = new NpgsqlCommand(query, con);
                reader = cmd.ExecuteReader();
                try
                {
                    for (int i = 0; i < tab.Length; i++)
                    {
                        try
                        {
                            reader.Read();
                            for (int u = 0; u < tabPropGeom.Length; u++)
                            {
                                int fieldCount = reader.FieldCount;
                                string geo = reader.GetOrdinal(tabPropGeom[u].Name).ToString();
                                geo = reader[tabPropGeom[u].Name].ToString();
                                Geometry geometry = new Geometry(reader[tabPropGeom[u].Name].ToString());
                                geo = geometry.Geo;
                                string fieldName = tabPropGeom[u].Name;
                                fieldName = fieldName;
                                tabPropGeom[u].SetValue(tab[i], geometry, null);
                            }
                        }
                        catch (Exception e2) {
                            throw new Exception("u : " + e2.Message);
                        }
                        

                    }
                }
                catch (Exception e1) {
                    throw new Exception("i : " + e1.Message);
                }
                
            }
            catch (Exception e) {
                throw (e);
            }

            return tab;
        }*/

        public object[] multiFind(object o, string nomTable, string apsWhere, NpgsqlConnection con)
        {
            NpgsqlDataReader reader = null;
            NpgsqlCommand cmd = null;
            bool existGeom = false;
            try
            {
                string requete = "select * from " + nomTable;
                if (apsWhere != null)
                {
                    requete += " where " + apsWhere;
                }
                cmd = new NpgsqlCommand(requete, con);
                reader = cmd.ExecuteReader();
                Type objectType = o.GetType();
                PropertyInfo[] properties = objectType.GetProperties(); 
                List<object> result = new List<object>();
                while (reader.Read())
                {
                    object temporaire = Activator.CreateInstance(objectType);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (!string.Equals("Geometry", properties[i].GetType().Name, StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name], null);
                            }
                            catch (Exception ex) {
                                existGeom = true;
                            }
                        }
                        else {
                            existGeom = true;
                        }
                    }
                    //compteurBoucle++;
                    result.Add(temporaire);
                }
                object[] retour = result.ToArray();
                /*if (existGeom) {
                    retour = getGeom(retour, nomTable, apsWhere, con);
                }*/
                
                return retour;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally {
                reader.Close();
            }

        }

        public object[] executeSelectQuery(string requete, object o, NpgsqlConnection con)
        {
            NpgsqlDataReader reader = null;
            NpgsqlCommand cmd = null;
            bool existGeom = false;
            try
            {
                cmd = new NpgsqlCommand(requete, con);
                reader = cmd.ExecuteReader();
                Type objectType = o.GetType();
                PropertyInfo[] properties = objectType.GetProperties();
                List<object> result = new List<object>();
                while (reader.Read())
                {
                    object temporaire = Activator.CreateInstance(objectType);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (!string.Equals("Geometry", properties[i].GetType().Name, StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name], null);
                            }
                            catch (Exception ex)
                            {
                                existGeom = true;
                            }
                        }
                        else
                        {
                            existGeom = true;
                        }
                    }
                    //compteurBoucle++;
                    result.Add(temporaire);
                }
                object[] retour = result.ToArray();
                /*if (existGeom) {
                    retour = getGeom(retour, nomTable, apsWhere, con);
                }*/

                return retour;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                reader.Close();
            }

        }

        public string dateTimeTotimeSql(DateTime time)
        {
            int hour = time.Hour;
            int minute = time.Minute;
            int second = time.Second;
            int millisecond = time.Millisecond;
            string result = "'" + Utility.to2char(hour) + ":" + Utility.to2char(minute) + ":" + Utility.to2char(second) + "." + "000'";

            return result;
        }

        public static bool insert(object obj, string nomTable){
            Boolean succes = false ;
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                sqlUtility.insertObject(obj, nomTable, connection);
                succes = true;
            }
            catch (Exception e)
            {
                throw (e);
                succes = false;
            }
            finally {
                connection.Close();
            }

            return succes;
        }

        public string insertObject(object obj, string nomTable, NpgsqlConnection connexion)
        {

            // Start a local transaction.
            NpgsqlTransaction tran = connexion.BeginTransaction();

            string request = "";
            try
            {
                Type type = obj.GetType();
                //FieldInfo[] tabFields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

                PropertyInfo[] properties = type.GetProperties();
                ///Insertion dans la base de donnée

                request = "INSERT INTO " + nomTable + "(";
                for (int i = 0; i < properties.Length; i++)
                {
                    if (i != properties.Length - 1) request += "" + properties[i].Name + ",";
                    else request += "" + properties[i].Name + ")";
                }
                request += " VALUES (";


                for (int i = 0; i < properties.Length; i++)
                {
                    if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase) || string.Equals("timespan", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        if (i == properties.Length - 1) request += "'" + properties[i].GetValue(obj, null) + "')";
                        else request += "'" + properties[i].GetValue(obj, null) + "',";
                    }
                    else if (string.Equals("DateTime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        DateTime dt = (DateTime)properties[i].GetValue(obj, null);
                        if (i == properties.Length - 1) request += dateTimeTotimeSql(dt) + ")";
                        else request += dateTimeTotimeSql(dt) + ",";
                    }
                    else if (string.Equals("GeoCoordinate", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        GeoCoordinate  latlng = (GeoCoordinate) properties[i].GetValue(obj, null);
                        request += "ST_GeomFromText('POINT(" + latlng.Latitude + " " + latlng.Longitude + ")')";
                        if (i == properties.Length - 1) request += ")";
                        else request += ",";
                    }
                    else
                    {
                        if (i == properties.Length - 1) request += "" + properties[i].GetValue(obj, null) + ")";
                        else request += "" + properties[i].GetValue(obj, null) + ",";
                    }
                }

                NpgsqlCommand command = new NpgsqlCommand(request, connexion);
                command.Transaction = tran;
                Console.WriteLine(request);
                command.ExecuteNonQuery();
                tran.Commit();
            }
            catch (Exception exp)
            {
                tran.Rollback();

            }
            return request;

        }

        public NpgsqlTransaction insertObject(object obj, string nomTable, NpgsqlConnection connexion, NpgsqlTransaction tran)
        {  
            string request = "";
            try
            {
                Type type = obj.GetType();
                //FieldInfo[] tabFields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

                PropertyInfo[] properties = type.GetProperties();
                ///Insertion dans la base de donnée

                request = "INSERT INTO " + nomTable + "(";
                for (int i = 0; i < properties.Length; i++)
                {
                    if (i != properties.Length - 1) request += "" + properties[i].Name + ",";
                    else request += "" + properties[i].Name + ")";
                }
                request += " VALUES (";


                for (int i = 0; i < properties.Length; i++)
                {
                    if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase) || string.Equals("timespan", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        if (i == properties.Length - 1) request += "'" + properties[i].GetValue(obj, null) + "')";
                        else request += "'" + properties[i].GetValue(obj, null) + "',";
                    }
                    else if (string.Equals("DateTime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        DateTime dt = (DateTime)properties[i].GetValue(obj, null);
                        if (i == properties.Length - 1) request += dateTimeTotimeSql(dt) + ")";
                        else request += dateTimeTotimeSql(dt) + ",";
                    }
                    else if (string.Equals("GeoCoordinate", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        GeoCoordinate latlng = (GeoCoordinate)properties[i].GetValue(obj, null);
                        request += "ST_GeomFromText('POINT(" + latlng.Latitude + " " + latlng.Longitude + ")')";
                        if (i == properties.Length - 1) request += ")";
                        else request += ",";
                    }
                    else
                    {
                        if (i == properties.Length - 1) request += "" + properties[i].GetValue(obj, null) + ")";
                        else request += "" + properties[i].GetValue(obj, null) + ",";
                    }
                }

                NpgsqlCommand command = new NpgsqlCommand(request, connexion);
                command.Transaction = tran;
                Console.WriteLine(request);
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                //throw (exp);
                throw new Exception(request);
            }
            return tran;
        }

    }
}