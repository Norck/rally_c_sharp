﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class TempsLiaison
    {
        private string esDepart;
        private string esArrive;
        private DateTime temps;
        private string idEquipage;

        public TempsLiaison() { }

        public TempsLiaison(string esDpart, string esArrive, DateTime temps, string idEquipage) {
            this.EsDepart = esDepart;
            this.EsArrive = esArrive;
            this.Temps = temps;
            this.IdEquipage = idEquipage;
        }

        public string EsDepart {
            get {
                return esDepart;
            }
            set {
                this.esDepart = esDepart;
            }
        }
        public string EsArrive {
            get {
                return esArrive;
            }
            set {
                this.esArrive = value;
            }
        }
        public DateTime Temps {
            get {
                return temps;
            }
            set {
                this.temps = value;
            }
        }
        public string IdEquipage {
            get {
                return idEquipage;
            }
            set {
                this.idEquipage = value;
            }
        }
    }
}