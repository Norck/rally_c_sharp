﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class Geometry
    {
        private string geo;

        public string Geo{ 
            get{
                return geo;
            }
            set{
                geo = value;
            }
        }
        public Geometry(string geo) {
            this.Geo = geo;
        }
    }
}