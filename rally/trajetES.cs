﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Device.Location;
using Npgsql;

namespace rally
{
    public class TrajetES
    {
        private string idES;
        private GeoCoordinate latlng;

        

        public string IdES{
            get{
                return idES;
            }
            set{
                idES = value;
            }
        }

        public GeoCoordinate Latlng{
            get{
                return latlng;
            }
            set{
                latlng = value;
            }
        }

        public TrajetES(string idES, GeoCoordinate latlng) {
            this.IdES = idES;
            this.Latlng = latlng;
        }

        public string insert() {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            string request = "";
            try
            {
                connection = dbConnect.connect();
               request = sqlUtility.insertObject(this, "trajetES", connection);
            }
            catch (Exception e)
            {
                //throw (e);
            }
            finally {
                connection.Close();
            }
            return request;
        }
    }

}