﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tempsES.aspx.cs" Inherits="rally.tempsES" %>

<%@ Import Namespace="rally" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
	    integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
	    crossorigin=""></script>
    <script src="leaflet.geometryutil.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Formulaire insertion temps ES</h1>
        <asp:TextBox ID="txtPath" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="distance" runat="server" Text=""></asp:TextBox>
		<input id="insertPath" type="button" name="insertPath" onclick="insertPath()" value="insertPath">	
        <input id="clearPath" type="button" name="clearPath" onclick="clearPath()" value="clearPath">
        <div id="mapid" style="width: 600px; height: 400px;"></div>
        
            <table>
                <tr>
                    <td></td>
                    <td><div ID="message" runat="server"></div></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label5" runat="server" Text="rally"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListRally" runat="server" 
                            onselectedindexchanged="dropDownListRally_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text="idES"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListES" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="dropDownListES_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server" Text="idEquipage"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListIdEquipage" runat="server" 
                            AutoPostBack="True" 
                            onselectedindexchanged="dropDownListIdEquipage_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>
                <tr>
                    <td></td>
                </tr>
                </tr>
                <tr><td><b>Insertion unique</b></td></tr>
                <tr>
                <td><asp:Label ID="Label4" runat="server" Text="tempsES"></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:Label ID="labelUniqueTemps" runat="server" Text=""></asp:Label></td>
                    <td><asp:TextBox ID="txtUniqueTemps" runat="server" Text="" ></asp:TextBox></td>
                    <td>
                        <asp:Button ID="btnInsert" runat="server" Text="insert" 
                            onclick="btnInsert_Click"/>
                    </td>
                </tr>

                <tr><td><asp:TextBox ID="essaie" runat="server" Text=""></asp:TextBox></td></tr>
                <tr><td><b>Insertion multiple</b></td></tr>
                
            </table>
            <asp:PlaceHolder ID="Panel1" runat="server" EnableViewState="true"></asp:PlaceHolder>  
            <p><asp:Button ID="Button1" runat="server" Text="insert" onclick="Button1_Click" 
                    style="height: 26px"/></p>
    </div>
    </form>

    <script src="map.js"></script>
</body>
</html>
