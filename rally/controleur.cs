﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Controleur
    {
        private string nom;
        private string mdp;

        public string Nom {
            get {
                return nom;
            }
            set {
                nom = value;
            }
        }
        public string Mdp {
            get {
                return mdp;
            }
            set {
                mdp = value;
            }
        }
    }
    
}