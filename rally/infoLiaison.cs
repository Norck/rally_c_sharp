﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class InfoLiaison
    {
        private string nomRally;
        private string esDepart;
        private string esArrive;
        private DateTime temps;
        private string idEquipage;
        private DateTime tempsOptimal;

        public InfoLiaison() { }

        public InfoLiaison(string nomRally, string esDpart, string esArrive, DateTime temps, string idEquipage, DateTime tempsOptimal)
        {
            this.nomRally = nomRally;
            this.EsDepart = esDepart;
            this.EsArrive = esArrive;
            this.Temps = temps;
            this.IdEquipage = idEquipage;
            this.TempsOptimal = tempsOptimal;
        }
        public string NomRally {
            get {
                return nomRally;
            }
            set {
                this.nomRally = value;
            }
        }

        public DateTime TempsOptimal{
            get{
                return tempsOptimal;
            }
            set{
                this.tempsOptimal = value;
            }
        }

        public string EsDepart
        {
            get
            {
                return esDepart;
            }
            set
            {
                this.esDepart = esDepart;
            }
        }
        public string EsArrive
        {
            get
            {
                return esArrive;
            }
            set
            {
                this.esArrive = value;
            }
        }
        public DateTime Temps
        {
            get
            {
                return temps;
            }
            set
            {
                this.temps = value;
            }
        }
        public string IdEquipage
        {
            get
            {
                return idEquipage;
            }
            set
            {
                this.idEquipage = value;
            }
        }
    }
}