﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace rally
{
    public partial class championat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertChampionat_Click(object sender, EventArgs e)
        {
            Championat championat = new Championat(txtNomChampionat.Text);
            try
            {
                championat.insert(txtNomRally.Text);
                erreurChampionat.InnerHtml = "";
            }
            catch (Exception ex) {
                erreurChampionat.InnerHtml = ex.Message;
                //erreurChampionat.InnerHtml = "Un ou plusieurs rallys sont déjà existants.";
            }
        }
    }
}