﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class ParticipationES
    {
        private string idES;
        private string idEquipage;
        private string idVoiture;

        public ParticipationES() { }
        public ParticipationES(string idES, string idEquipage, string idVoiture){
            this.IdES = idES;
            this.IdEquipage = idEquipage;
            this.IdVoiture = idVoiture;
        }

        public string IdES{
            get{
                return idES;
            }
            set{
                idES = value;
            }
        }

        public string IdEquipage {
            get {
                return idEquipage;
            }
            set {
                idEquipage = value;
            }
        }

        public string IdVoiture {
            get {
                return idVoiture;
            }
            set {
                idVoiture = value;
            }
        }

        public string insert(){
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility util = new SqlUtility();
            string query = null;
            try{
                connection = dbConnect.connect();
                query = util.insertObject(this, "participationES", connection);
            }catch(Exception e){
                throw (e);
            }finally{
                connection.Close();
            }
            return query;
         }

        public Object[] getAll() {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility util = new SqlUtility();
            object[] tabParticipation = null;
            try
            {
                connection = dbConnect.connect();
                tabParticipation = util.multiFind(this, "participationES", null, connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally {
                connection.Close();
            }
            return tabParticipation;
        }

        public object[] getEquipageNonInscrit(string idES)
        {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility util = new SqlUtility();
            object[] tabEquipage = null;
            try
            {
                connection = dbConnect.connect();
                tabEquipage = util.multiFind(new Equipage(), "equipage", "idEquipage NOT IN(SELECT idEquipage FROM participationES WHERE idES = '" + idES + "')", connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                connection.Close();
            }
            return tabEquipage;
        }

        public object[] getEquipageInscrit(string idES)
        {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility util = new SqlUtility();
            object[] tabEquipage = null;
            try
            {
                connection = dbConnect.connect();
                string query = "SELECT equipage.* FROM participationES JOIN equipage ON participationES.idEquipage = equipage.idEquipage WHERE idES='" + idES + "'";
                tabEquipage = util.executeSelectQuery(query, new Equipage(), connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                connection.Close();
            }

            return tabEquipage;
        }

        public object[] getEquipageInscritSansTemps(string idES)
        {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility util = new SqlUtility();
            object[] tabEquipage = null;
            try
            {
                connection = dbConnect.connect();
                string query = "SELECT equipage.* FROM participationES JOIN equipage ON participationES.idEquipage = equipage.idEquipage WHERE idES='" + idES + "' AND participationES.idEquipage NOT IN (SELECT idEquipage FROM tempsES)";
                tabEquipage = util.executeSelectQuery(query, new Equipage(), connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                connection.Close();
            }

            return tabEquipage;
        }

    }
}