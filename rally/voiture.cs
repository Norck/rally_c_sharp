﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Voiture
    {
        private string idVoiture;
        private string idCategorieVoiture;
        private string nomVoiture;
        private string possesseur;

        public Voiture() { }

        public Voiture(string idVoiture, string idCategorieVoiture,
            string nomVoiture, string possesseur) {
            this.IdVoiture = idVoiture;
            this.IdCategorieVoiture = idCategorieVoiture;
            this.NomVoiture = nomVoiture;
            this.Possesseur = possesseur;
        }

        public string IdVoiture{
            get{
                return idVoiture;
            }
            set{
                idVoiture = value;
            }
        }
        public string IdCategorieVoiture {
            get {
                return idCategorieVoiture;
            }
            set {
                idCategorieVoiture = value;
            }
        }
        public string NomVoiture {
            get {
                return nomVoiture;
            }
            set {
                nomVoiture = value;
            }
        }
        public string Possesseur {
            get {
                return possesseur;
            }
            set {
                possesseur = value;
            }
        }
        public bool isCarUsedByOther(string idCar, string nomRally, string idEquipage) {
            bool used = false;
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                string query = "SELECT voiture.* FROM participationES JOIN epreuveSpecial ON participationES.idES = epreuveSpecial.idES JOIN rally ON epreuveSpecial.nomRally = rally.nomRally JOIN voiture ON participationES.idVoiture = voiture.idVoiture WHERE voiture.idVoiture = '" + idCar + "' AND participationES.idEquipage != '" + idEquipage + "'LIMIT 1";
                object[] result = sqlUtility.executeSelectQuery(query, this, connection);
                if (result.Length > 0)
                {
                    used = true;
                }
            }
            catch (Exception e) { 
                
            }
            return used;
        }

    }
}