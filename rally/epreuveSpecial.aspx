﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="epreuveSpecial.aspx.cs" Inherits="rally.epreuveSpecial" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
	    integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
	    crossorigin=""></script>
    <script src="leaflet.geometryutil.js"></script>
    <style>
            
    </style>
</head>
<body>
    <div ID="Div1" runat="server"></div>
    <h1>Formulaire insertion épreuve spécial</h1>
    <div ID="message" runat="server"></div>

    <form id="form1" runat="server">
        
        <asp:TextBox ID="txtPath" runat="server" Text="" 
            ontextchanged="txtPath_TextChanged"></asp:TextBox>
        <asp:TextBox ID="distance" runat="server" Text=""></asp:TextBox>
		<input id="insertPath" type="button" name="insertPath" onclick="insertPath()" value="insertPath">	
        <input id="clearPath" type="button" name="clearPath" onclick="clearPath()" value="clearPath">
        <div id="mapid" style="width: 600px; height: 400px;"></div>
        
        
        <div>
            <table>
            <tr>
                <td></td>
                <td><div ID="errorListRally" runat="server"></div></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelDPLR" runat="server" Text="nomRally"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropDownListRally" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Numéro de l'épreuve"></asp:Label>
                </td>
                <td>
                <div ID="errorNumES" runat="server"></div>
                    <asp:DropDownList ID="dropDownListNumES" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Temps idéal"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTempsIdeal" runat="server" Text=""></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnInsertES" runat="server" Text="Valider" 
                        onclick="btnInsertES_Click" />
                </td>
            </tr>
            </table>
            <p>__________________________________________________</p>
            <table>

            <h1>Affichage classement</h1>
            <table>
                <tr>
                    <td>nomChampionat</td>
                    <td>
                        <asp:TextBox ID="txtNomChampionat" runat="server" Text=""></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="bntGetClassementChamp" runat="server" 
                            Text="getClassementChampionat" onclick="bntGetClassementChamp_Click"/>
                    </td>
                </tr>
			    <tr>
                    <td>
					    <asp:Label ID="Label8" runat="server" Text="nomRallye"></asp:Label>
				    </td>
				    <td>
                        <asp:TextBox ID="txtNomRallye" runat="server" Text=""></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGetListES" runat="server" Text="getListES" 
                            onclick="btnGetListES_Click"/>
                    </td>
                    <td><asp:Label ID="Label4" runat="server" Text="listES"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="dropDownListES" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnGetClassement" runat="server" Text="getClassementES" 
                            onclick="btnGetClassement_Click"/>
                    </td>
                    <td>
                        <asp:Button ID="btnGetClassementRally" runat="server" Text="getClassementRally" 
                            onclick="btnGetClassementRally_Click"/>
                    </td>
                    <td>
                        <asp:Button ID="btnGetClassementChampionat" runat="server" Text="getClassementChampionat"/>
                    </td>
                    <td>
                        <asp:Button ID="btnGetClassementRallyCateg" runat="server" Text="getClassementRallyCateg" 
                            onclick="btnGetClassementRally_Click"/>
                    </td>
			    </tr>
            </table>
        </div>
    </form>
    
    <div ID="htmlTabES" runat="server"></div>

    <div ID="htmlClassementRally" runat="server"></div>

    <script src="map.js"></script>

</body>
</html>
