﻿using System;
using Npgsql;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace rally
{
    public partial class participationES : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
            }
        }

        
        public void peuplerRally(){
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            dropDownListRally.Items.Clear();
            try
            {
                connection = dbConnect.connect();
                object[] tabRally = sqlUtility.multiFind(new Rally(), "rally", null, connection);
                for (int i = 0; i < tabRally.Length; i++)
                {
                    Rally temp = (Rally)tabRally[i];
                    dropDownListRally.Items.Add(new ListItem(temp.NomRally));
                }
            }
            catch (Exception e2)
            {
                throw (e2);
            }
            finally
            {
                connection.Close();
            }
        }

        public void peuplerES()
        {
            EpreuveSpecial ES = new EpreuveSpecial();
            object[] tabES = ES.get(dropDownListRally.SelectedValue);
            dropDownListES.Items.Clear();
            for (int i = 0; i < tabES.Length; i++)
            {
                EpreuveSpecial temp = (EpreuveSpecial)tabES[i];
                dropDownListES.Items.Add(new ListItem(temp.IdES));
            }
        }

        public void Insert(){

            ParticipationES part = new ParticipationES(dropDownListES.SelectedValue, dropDownListIdEquipage.SelectedValue, txtIdVoiture.Text);

            Voiture voiture = new Voiture();
            bool alreadyUsedByOther = voiture.isCarUsedByOther(part.IdVoiture, dropDownListRally.SelectedValue, part.IdEquipage);
            if (alreadyUsedByOther)
            {
                message.InnerHtml = "<p>erreur: La voiture est déjà utilisée.</p>";
            }
            else {
                message.InnerHtml = "<p>message: Insertion de participationES réussite.</p>";
            }
            
            
        }

        protected void btnClear_Click1(object sender, EventArgs e)
        {
            Clear();
        }

        public void Clear() {
            txtIdVoiture.Text = "";
        }


        protected void btnInsert_Click(object sender, EventArgs e)
        {
            Insert();
        }

        protected void btnGetAll_Click(object sender, EventArgs e)
        {
            ParticipationES part = new ParticipationES();
            Utility utility = new Utility();
            Object[] tabPart = part.getAll();
            Table tab = utility.generateTable(tabPart, new string[]{"idEs", "idEquipage", "idVoiture"}, new string[]{"idES", "idEquipage", "idVoiture"});
            form1.Controls.Add(tab);
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {

        }

        protected void dropDownListRally_SelectedIndexChanged(object sender, EventArgs e)
        {
            peuplerES();
            peuplerEquipage();
        }

        public void peuplerEquipage() {
            dropDownListIdEquipage.Items.Clear();
            object[] tabEquipage = new ParticipationES().getEquipageNonInscrit(dropDownListES.SelectedValue);
            for (int i = 0; i < tabEquipage.Length; i++) {
                Equipage temp = (Equipage)tabEquipage[i];
                dropDownListIdEquipage.Items.Add(new ListItem(temp.IdEquipage));
            }
        }
        
        protected void btnGetListRally_Click(object sender, EventArgs e)
        {
            peuplerRally();
            peuplerES();
            peuplerEquipage();
            
        }

        protected void dropDownListES_SelectedIndexChanged(object sender, EventArgs e)
        {
            peuplerEquipage();
        }
            
    }
}