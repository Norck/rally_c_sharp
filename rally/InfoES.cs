﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class InfoES
    {
        private string nomRally;
        private string idES;
        private string numES;
        private string idEquipage;
        private DateTime temps;
        private DateTime tempsIdeal;
        private int distance;
        private string sexe;
        private string idCategorieVoiture;

        public InfoES() { }

        public InfoES(string nomRally, string idES, string numES, string idEquipage, DateTime temps, DateTime tempsIdeal, int distance)
        {
            this.NomRally = nomRally;
            this.IdES = idES;
            this.NumES = numES;
            this.IdEquipage = idEquipage;
            this.Temps = temps;
            this.TempsIdeal = tempsIdeal;
            this.Distance = distance;
        }

        public string NomRally {
            get {
                return nomRally;
            }
            set {
                nomRally = value;
            }
        }
        public string IdES {
            get {
                return idES;
            }
            set {
                idES = value;
            }
        }
        public string NumES {
            get {
                return numES;
            }
            set {
                numES = value;
            }
        }
        public string IdEquipage {
            get {
                return idEquipage;
            }
            set {
                idEquipage = value;
            }
        }
        public DateTime Temps{
            get {
                return temps;
            }
            set {
                temps = value;
            }
        }
        public DateTime TempsIdeal {
            get {
                return tempsIdeal;
            }
            set {
                tempsIdeal = value;
            }
        }
        public int Distance {
            get {
                return distance;
            }
            set {
                distance = value;
            }
        }
        public string Sexe {
            get { 
                return sexe;
            }
            set {
                sexe = value;
            }
        }
        public string IdCategorieVoiture {
            get {
                return idCategorieVoiture;
            }
            set {
                idCategorieVoiture = value;
            }
        }
    }
}