﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace rally
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void insertRally_Click(object sender, EventArgs e)
        {
            Response.Redirect("/rally.aspx");
        }

        protected void insertES_Click(object sender, EventArgs e)
        {
            Response.Redirect("/epreuveSpecial.aspx");
        }

        protected void insertEquipage_Click(object sender, EventArgs e)
        {
            Response.Redirect("/equipage.aspx");
        }

        protected void insertPartES_Click(object sender, EventArgs e)
        {
            Response.Redirect("/participationES.aspx");
        }

        protected void btnInsertPiloteCopilote_Click(object sender, EventArgs e)
        {
            Response.Redirect("/pilote.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("/epreuveSpecial.aspx");
        }

        protected void insertTemps_Click(object sender, EventArgs e)
        {
            Response.Redirect("/tempsES.aspx");
        }

        protected void insertChampionat_Click(object sender, EventArgs e)
        {
            Response.Redirect("/championat.aspx");
        }

        protected void affichClassement_Click(object sender, EventArgs e)
        {
            Response.Redirect("/epreuveSpecial.aspx");
        }

        protected void btnLog_Click(object sender, EventArgs e)
        {
            Response.Redirect("/log.aspx");
        }
    }
}