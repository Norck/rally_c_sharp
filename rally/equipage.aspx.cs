﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Npgsql;

namespace rally
{
    public partial class equipage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                peuplerDDL(sender, e);    
            }
        }

        public void peuplerDDL(object sender, EventArgs e) {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();

            dropDownListPilote.Items.Clear();
            dropDownListCopilote.Items.Clear();

            try
            {
                connection = dbConnect.connect();
                object[] tabPilote = sqlUtility.multiFind(new Pilote(), "pilote", null, connection);
                object[] tabCopilote = sqlUtility.multiFind(new Copilote(), "copilote", null, connection);
                for (int i = 0; i < tabPilote.Length; i++)
                {
                    Pilote temp = (Pilote)tabPilote[i];
                    dropDownListPilote.Items.Add(new ListItem(temp.NomPilote));
                }
                for (int i = 0; i < tabCopilote.Length; i++)
                {
                    Copilote temp = (Copilote)tabCopilote[i];
                    dropDownListCopilote.Items.Add(new ListItem(temp.NomCopilote));
                }
            }
            catch (Exception e2)
            {
                throw (e2);
            }
            finally
            {
                connection.Close();
            }
        }

        protected void btnInsertEquipage_Click(object sender, EventArgs e)
        {


            Equipage equipage = new Equipage(dropDownListPilote.SelectedValue, dropDownListCopilote.SelectedValue);
            bool success = false;
            try
            {
                success = SqlUtility.insert(equipage, "equipage");
            }
            catch (Exception ex) {
                success = false;
            }
            
            if (success)
            {
                message.InnerHtml = "<p>Insertion equipage réussite.</p>";
            }
            else {
                message.InnerHtml = "<p>Cette equipage existe déjà:</p>";
            }
        }

    }
}