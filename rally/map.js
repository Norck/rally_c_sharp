﻿
var mymap = L.map('mapid').setView([-18.920147, 46.527409], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var initLines = [84.999893, -135.002479]
var tabPoint = [[84.999893, -135.002479]];
var tabTime = [];
var lines = new L.polyline(tabPoint).addTo(mymap);
var distance = 0;

function drawPath(e) {
    var marker = L.marker(e.latlng).addTo(mymap);
    if (tabPoint[0].lat == initLines.lat && tabPoint[0].lng == initLines.lng) {
        tabPoint.pop();
    }
    tabTime.push(new Date());
    window.mymap.removeLayer(window.lines);
    tabPoint.push(e.latlng);
    var lines = new L.polyline(tabPoint).addTo(mymap);
}

function insertPath() {
    var points = "";
    for (var i = 0; i < tabTime.length; i++) {
        points += tabPoint[i].lat + "," + tabPoint[i].lng + "|" + tabTime[i].getHours() + ":" + tabTime[i].getMinutes() + ":" + tabTime[i].getSeconds() + "." + tabTime[i].getMilliseconds();
        if (i < tabPoint.length - 1) {
            points += "/";
        }
    }
    distance = 0;
    for (var i = 0; i < tabPoint.length - 1; i++) {
        distance += tabPoint[i].distanceTo(tabPoint[i+1]);
    }
    document.getElementById('distance').value = distance;
    document.getElementById('txtPath').value = points;
}

function clearPath() {
    mymap.clearLayers();
}

mymap.on('click', drawPath);
document.getElementById('insertPath').onclick = insertPath;