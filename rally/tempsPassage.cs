﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Device.Location;
using Npgsql;

namespace rally
{
    public class TempsPassage : TempsES
    {
        private GeoCoordinate latlng;

        public GeoCoordinate LatLng {
            get {
                return latlng;
            }
            set {
                latlng = value;
            }
        }
        public TempsPassage(string idES, string idEquipage, DateTime temps, int confirmation, GeoCoordinate latlng) {
            this.IdES = idES;
            this.IdEquipage = idEquipage;
            this.Temps = temps;
            this.LatLng = latlng;
        }

        public TempsPassage[] getTime(string data)
        {

            string[] tabPoint = data.Split('/');
            TempsPassage[] result = new TempsPassage[tabPoint.Length];

            for (int i = 0; i < tabPoint.Length; i++)
            {
                string[] temp = tabPoint[i].Split('|');
                string[] latlng = temp[0].Split(',');
                double lat = double.Parse(latlng[0]);
                double lng = double.Parse(latlng[1]);
                DateTime time = Utility.stringToDate(temp[1]);
                result[i] = new TempsPassage("a", "a", time, 1, new GeoCoordinate(lat, lng));
            }

            return result;

        }

        public TempsPassage() { }
    }
}