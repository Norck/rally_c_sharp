﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Championat
    {
        private string nomChampionat;

        public string NomChampionat {
            get {
                return nomChampionat;
            }
            set {
                this.nomChampionat = value;
            }
        }
        public Championat() { }
        public Championat(string nomChampionat) {
            this.NomChampionat = nomChampionat;
        }

        public void insert(string listRally) {
            string exception = "";
            DbConnect dbConnect = new DbConnect();
            NpgsqlConnection connection = null;
            NpgsqlTransaction transaction = null;
            SqlUtility sqlUtility = new SqlUtility();
            
            try
            {
                connection = dbConnect.connect();
                transaction = connection.BeginTransaction();
                object[] dbDuplicate = sqlUtility.multiFind(this, "championat", "nomChampionat='" + this.NomChampionat + "'", connection);
                if (dbDuplicate.Length > 0) {
                    throw new Exception("championat déjà existant.");
                }
                transaction = sqlUtility.insertObject(this, "championat", connection, transaction);
                if (!string.Equals("", listRally)) {
                    string[] tabRally = listRally.Split('/');
                    for (int i = 0; i < tabRally.Length; i++) {
                        Rally temp = new Rally(tabRally[i], this.NomChampionat);
                        transaction = sqlUtility.insertObject(temp, "rally", connection, transaction);
                    }
                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw new Exception (exception + e.Message);
            }
            finally {
                transaction.Dispose();
                connection.Close();
            }
        }

        public Classement[] getClassement() {
            DbConnect dbConnect = new DbConnect();
            NpgsqlConnection connection = null;
            SqlUtility sqlUtility = new SqlUtility();
            Classement[] result = new Classement[0];
            string exception = "a1";
            try
            {
                connection = dbConnect.connect();
                object[] dbRally = sqlUtility.multiFind(new Rally(), "rally", "nomChampionat='" + nomChampionat + "'", connection);
                Classement[][] clChampionat = new Classement[dbRally.Length][];//misy blem ato fa tsy aiko hoe inona
                for (int i = 0; i < dbRally.Length; i++)
                {
                    
                    Rally temp = (Rally)dbRally[i];
                    clChampionat[i] = new Classement[0];
                    clChampionat[i] = new EpreuveSpecial().getClassementRally(temp.NomRally);
                    exception += "i=" + i.ToString();
                    clChampionat[i] = clChampionat[i][0].appliquerPenalite(clChampionat[i]);
                    
                    
                }
                /*
                 for (int i = 0; i < dbRally.Length; i++)
                {
                    exception += "i=" + i.ToString();
                    Rally temp = (Rally)dbRally[i];
                    clChampionat[i] = new Classement[0];
                    clChampionat[i] = new EpreuveSpecial().getClassementRally(temp.NomRally);
                    clChampionat[i] = clChampionat[i][0].appliquerPenalite(clChampionat[i]);
                    
                    
                }
                 */
                exception += "a3";
                for (int i = 0; i < clChampionat[0].Length; i++)
                {
                    for (int u = 1; u < clChampionat.Length; u++)
                    {
                        for (int v = 0; v < clChampionat[u].Length; v++)
                        {
                            if (string.Equals(clChampionat[0][i].IdEquipage, clChampionat[u][v].IdEquipage))
                            {
                                clChampionat[0][i].STemps += clChampionat[u][v].STemps;
                                clChampionat[0][i].SPenalite += clChampionat[u][v].SPenalite;
                                clChampionat[0][i].STempsAvecPenalite += clChampionat[u][v].STempsAvecPenalite;
                                clChampionat[0][i].Distance += clChampionat[u][v].Distance;
                                clChampionat[0][i].Point += clChampionat[u][v].Point;
                                break;
                            }
                        }
                    }
                }
                exception += "a4";
                clChampionat[0] = new Classement().trieTemps2(clChampionat[0]);
                for (int i = 0; i < clChampionat.Length; i++)
                {
                    clChampionat[0][i].Rang = i + 1;
                    clChampionat[0][i].calculVitesseMoyenne();
                }
                result = clChampionat[0];
            }
            catch (Exception e)
            {
                throw new Exception(exception + e.Message);
            }
            finally {
                connection.Close();
            }

            return result;
        }
    }
}