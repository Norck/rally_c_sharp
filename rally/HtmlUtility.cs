﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace rally
{
    public class HtmlUtility
    {
        public string getHtml(object[] tabObj, string[] propToPrint) {
            string html = "";
            html = "<table width='800' border='1'>";

            //Les noms des colonnes
            Type type = tabObj[0].GetType();
            PropertyInfo[] prop = type.GetProperties();
            List<PropertyInfo> listProp = new List<PropertyInfo>();
            if (propToPrint != null) {
                for (int i = 0; i < prop.Length; i++)
                {
                    for (int u = 0; u < propToPrint.Length; u++)
                    {
                        if (string.Equals(prop[i].Name, propToPrint[u], StringComparison.OrdinalIgnoreCase))
                        {
                            listProp.Add(prop[i]);
                            break;
                        }
                    }
                }
                prop = listProp.ToArray();
            }
            
            html += "<tr>";
            for (int i = 0; i < prop.Length; i++) {
                html += "<th>" + prop[i].Name + "</th>";
            }
            html += "</tr>";
            for (int i = 0; i < tabObj.Length; i++) {
                html += "<tr>";
                for (int u = 0; u < prop.Length; u++) {
                    if(string.Equals(prop[u].PropertyType.Name, "DateTime", StringComparison.OrdinalIgnoreCase)){
                        DateTime date = (DateTime)prop[u].GetValue(tabObj[i], null);
                        html += "<td>" + Utility.to2char(date.Hour) + ":" + Utility.to2char(date.Minute) + ":" + Utility.to2char(date.Second) + "." + Utility.to2char(date.Millisecond) +"</td>";
                    }else{
                        html += "<td>" + prop[u].GetValue(tabObj[i], null) + "</td>";
                    }
                }
                html += "</tr>";
            }

            html += "</table>";

            return html;
        }
    }
}