﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="rally.index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td><asp:Button ID="btnLog" runat="server" 
                        Text="Se connecter" Height="26px" onclick="btnLog_Click"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnInsertPiloteCopilote" runat="server" 
                        Text="Insérer pilote ou copilote" Height="26px" 
                        onclick="btnInsertPiloteCopilote_Click"/>
                </td>
			</tr>
            <tr>
                <td>
                    <asp:Button ID="insertChampionat" runat="server" Text="Insérer championat" 
                        onclick="insertChampionat_Click" />
                </td>
            </tr>
                <td>
                    <asp:Button ID="insertRally" runat="server" Text="Insérer rally" 
                        onclick="insertRally_Click"/>
                </td>
			</tr>
            <tr>
                <td>
                    <asp:Button ID="affichClassement" runat="server" Text="Affichage classement" 
                        onclick="affichClassement_Click"/>
                </td>
			</tr>
            <tr>
                <td>
                    <asp:Button ID="insertES" runat="server" Text="Insérer Epreuve spécial" 
                        onclick="insertES_Click"/>
                </td>
			</tr>
            <tr>
                <td>
                    <asp:Button ID="insertEquipage" runat="server" Text="Insérer equipage" 
                        onclick="insertEquipage_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="insertPartES" runat="server" Text="Insérer participation ES" 
                        onclick="insertPartES_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="insertTemps" runat="server" Text="Insérer temps equipage" 
                        onclick="insertTemps_Click"/>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
