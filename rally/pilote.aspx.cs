﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Npgsql;

namespace rally
{
    public partial class pilote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dropDownListSexePilote.Items.Clear();
            dropDownListSexeCopilote.Items.Clear();

            dropDownListSexePilote.Items.Add(new ListItem("h"));
            dropDownListSexePilote.Items.Add(new ListItem("f"));
            dropDownListSexeCopilote.Items.Add(new ListItem("h"));
            dropDownListSexeCopilote.Items.Add(new ListItem("f"));
        }

        protected void btnInsertPilote_Click(object sender, EventArgs e)
        {
            Pilote pilote = new Pilote(txtPilote.Text, dropDownListSexePilote.SelectedValue);
            Boolean success = pilote.insert();
            if (success)
            {
                message.InnerHtml = "<p>Insertion pilote réussit.</p>";
            }
            else {
                message.InnerHtml = "<p>Ce pilote existe déjà.</p>";
            }
        }

        protected void btnInsertCopilote_Click(object sender, EventArgs e)
        {
            Copilote copilote = new Copilote(txtCopilote.Text, dropDownListSexeCopilote.SelectedValue);
            Boolean success = copilote.insert();
            if (success)
            {
                message.InnerHtml = "<p>Insertion copilote réussit.</p>";
            }
            else
            {
                message.InnerHtml = "<p>Ce copilote existe déjà.</p>";
            }
        }
    }
}