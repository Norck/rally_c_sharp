﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="championat.aspx.cs" Inherits="rally.championat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b>Formulaire insertion championat</b>
        <table>
            <tr>
                <td></td>
                <td><div ID="erreurChampionat" runat="server"></div></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label1" runat="server" Text="nomChampionat"></asp:Label></td>
                <td><asp:TextBox ID="txtNomChampionat" runat="server" Text=""></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label2" runat="server" Text="nomRally"></asp:Label></td>
                <td><asp:TextBox ID="txtNomRally" runat="server" Text=""></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnInsertChampionat" runat="server" Text="Valider" 
                        onclick="btnInsertChampionat_Click"/>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
