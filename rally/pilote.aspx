﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pilote.aspx.cs" Inherits="rally.pilote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <h1>Formulaire insertion pilote et copilote</h1>
    <form id="form1" runat="server">
    <div ID="message" runat="server"></div>
        <table>
            <tr>
                <th>Pilote</th>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label" runat="server" Text="nom"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPilote" runat="server" Text=""></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="sexe"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropDownListSexePilote" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnInsertPilote" runat="server" Text="Inserer pilote" 
                        onclick="btnInsertPilote_Click" />
                </td>
            </tr>
            <tr>
                <th>Copilote</th>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="nom"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCopilote" runat="server" Text=""></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="sexe"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropDownListSexeCopilote" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnInsertCopilote" runat="server" Text="Inserer copilote" 
                        onclick="btnInsertCopilote_Click" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
