﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class Test
    {
        private int number;

        public int Number
        {
            get
            {
                return Number;
            }
            set
            {
                number = value;
            }
        }
        public Test(int number) {
            this.Number = number;
        }

        public int[] trieCroissant(int[] tab) {
            for (int i = 0; i < tab.Length - 1; i++) {
                if(tab[i] > tab[i+1]){
                    int temp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = temp;

                    for (int u = i; u > 1; u--)
                    {
                        if (tab[u] < tab[u - 1])
                        {
                            temp = tab[u - 1];
                            tab[u - 1] = tab[u];
                            tab[u] = temp;
                        }
                        else {
                            break;
                        }
                    }
                }
            }
            return tab;
        }
    }
    
}