﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace rally
{
    public class Utility
    {
        /*public String printAddButton(object[] tab, string[] colonneInserer, string[] liens) { 
            
        }*/

        public char[] concat2CharArray(char[] tab1, char[] tab2) { 
            char[] result = new char[tab1.Length + tab2.Length];
            int i = 0, u = 0;
            for (i = 0; i < tab1.Length; i++) {
                result[i] = tab1[i];
            }
            for (u = 0; u < tab2.Length; u++) {
                result[i] = tab2[u];
                i++;
            }
            return result;
        }

        //minute sy seconde ihany no rarahin'ito
        public float dateTimeToSeconds(DateTime time) {
            float seconds = 0;
            int minute = time.Minute;
            int second = time.Second;
            int millisecond = time.Millisecond/1000;
            seconds = (minute * 60) + second + (millisecond/1000);

            return seconds;
        }

        //ito tsy manadino ny heure
        public float dateTimeToSeconds2(DateTime time)
        {
            float seconds = 0;
            float heure = time.Hour;
            float minute = (float) time.Minute;
            float second = (float) time.Second;
            float millisecond = (float) time.Millisecond / 1000;
            seconds = (heure * 3600) + (minute * 60) + second + (millisecond / 1000);

            return seconds ;
        }

        public DateTime addTime(DateTime date1, int secondPlus) {
            float second1 = dateTimeToSeconds2(date1);
            float totalSecond = second1 + secondPlus;
            DateTime result = new DateTime();

            int heure = (int) totalSecond / 3600;
            int minute = (int) (totalSecond - heure * 3600)/60;
            int seconde = (int) totalSecond - heure * 3600 - minute * 60;

            int int_second = (int)totalSecond;
            int millisecond = (int) (totalSecond - (float) int_second);
            try
            {
                result = new DateTime(date1.Year, date1.Month, date1.Day, heure, minute, seconde);
            }
            catch (Exception e) {
                throw new Exception("addTime heure = " + heure.ToString() + " minute = " + minute.ToString() + " seconde = " + seconde.ToString()  + e.Message);
            }
            return result;
        }

        public DateTime secondToDate(float totalSecond)
        {
            DateTime result = new DateTime();
            int heure = (int)totalSecond / 3600;
            int minute = (int)(totalSecond - heure * 3600) / 60;
            int seconde = (int)totalSecond - heure * 3600 - minute * 60;

            int int_second = (int)totalSecond;
            int millisecond = (int)(totalSecond - (float)int_second);
            try
            {
                result = new DateTime(2019, 12, 30, heure, minute, seconde, millisecond);
            }
            catch (Exception e)
            {
                throw new Exception("addTime heure = " + heure.ToString() + " minute = " + minute.ToString() + " seconde = " + seconde.ToString() + e.Message);
            }
            return result;
        }

        public Table generateTable(object[] data, string[] colonneInserer, string[] valeurEntete)
        {
            Table tab = new Table();
            Type type = data[0].GetType();
            PropertyInfo[] properties = type.GetProperties();
            //entete
            TableRow row = new TableRow();
            TableCell cell;
            for (int i = 0; i < valeurEntete.Length; i++)
            {
                cell = new TableCell();
                cell.Text = valeurEntete[i];
                cell.BorderWidth = 1;
                cell.Font.Bold = true;
                row.Cells.Add(cell);
            }
            tab.Rows.Add(row);
            //contenu
            for (int i = 0; i < data.Length; i++)
            {
                row = new TableRow();
                for (int j = 0; j < properties.Length; j++)
                {
                    for (int k = 0; k < colonneInserer.Length; k++)
                    {
                        if (properties[j].Name.Equals(colonneInserer[k], StringComparison.OrdinalIgnoreCase))
                        {
                            cell = new TableCell();
                            cell.Text = properties[j].GetValue(data[i], null).ToString();
                            cell.BorderWidth = 1;
                            row.Cells.Add(cell);
                            break;
                        }
                    }
                }
                tab.Rows.Add(row);
            }

            return tab;
        }

        public static string to2char(int num)
        {
            char[] charNum = num.ToString().ToCharArray();
            if (charNum.Length < 2)
            {
                char[] result = new char[2];
                result[0] = "0".ToCharArray()[0];
                result[1] = charNum[0];
                charNum = result;
            }

            return new String(charNum);
        }

        public static DateTime getTime1(string time){
            DateTime result = new DateTime();
            string[] tabTime = time.Split(':');
            if(tabTime.Length != 3){
                throw new Exception("format temps invalide.");
            }
            
            int heure = 0, minute = 0, seconde = 0, millisecond = 0;
            
            heure = Int16.Parse(tabTime[0]);
            minute = Int16.Parse(tabTime[1]);
            
            try{
                string[] tabSecond = tabTime[2].Split('.');
                seconde = Int16.Parse(tabSecond[0]);
                millisecond = Int16.Parse(tabSecond[1]);
            }catch(Exception e){
                seconde = Int16.Parse(tabTime[2]);
            }
            
            result = new DateTime(2019, 12, 30, heure, minute, seconde, millisecond);

            return result;
        }

        public static DateTime getTime2(string time) {
            DateTime result = new DateTime();
            string[] tabTime = time.Split(' ');
            if(tabTime.Length == 0){
                throw new Exception("format temps invalide."); 
            }
            int heure = 0, minute = 0, seconde = 0, millisecond = 0;
            heure = Int16.Parse(tabTime[0]);
            minute = Int16.Parse(tabTime[1]);
            seconde = Int16.Parse(tabTime[2]);
            if (tabTime.Length == 4) {
                millisecond = Int16.Parse(tabTime[3]);
            }
            result = new DateTime(2019, 12, 30, heure, minute, seconde, millisecond);

            return result;
        }

        public static DateTime getTime3(string time) {
            char[] tabTime = time.ToCharArray();
            DateTime result = new DateTime();
            int heure = 0, minute = 0, seconde = 0, millisecond = 0;
            
            heure = Int16.Parse(new string(new char[]{tabTime[0], tabTime[1]}));
            minute = Int16.Parse(new string(new char[]{tabTime[2], tabTime[3]}));
            seconde = Int16.Parse(new string(new char[]{tabTime[4], tabTime[5]}));

            if (tabTime.Length == 9) {
                millisecond = Int16.Parse(new string(new char[]{tabTime[6], tabTime[7], tabTime[8]}));
            }

            result = new DateTime(2019, 12, 30, heure, minute, seconde, millisecond);

            return result;
        }

        public int getDiffMinute(DateTime date1, DateTime date2) {
            float secondsLiaison = dateTimeToSeconds(date1);
            float secondsMin = dateTimeToSeconds(date2);
            float diff = secondsLiaison - secondsMin;
            if(diff < 0){
                diff = secondsMin - secondsLiaison;
            }
            float fMinute = diff / 60;
            int iMinute = (int)fMinute;
            float fiMinute = (float)iMinute;
            if (fiMinute < fMinute)
            {
                iMinute++;
            }
            return (int)iMinute;
        }

        public static DateTime stringToDate(string time) {
            DateTime result = new DateTime(2019, 12, 30, 1, 1, 1, 1);
            DateTime toCompare = result;
            Utility utility = new Utility();
            Type type = utility.GetType();
            for (int i = 1; i < 4; i++) {
                try
                {
                    MethodInfo method = type.GetMethod("getTime" + i.ToString());
                    result = (DateTime) method.Invoke(utility, new object[]{time});
                    break;
                }
                catch (Exception e) { }
            }
            if (DateTime.Compare(result, toCompare) == 0) {
                throw new Exception("castDate failed");
            }

            return result;
        }

        public DateTime getTemps(string temps){
            DateTime result = new DateTime();
            try{
            
            }catch(Exception e){
            
            }

            return result;
        }

    }
}