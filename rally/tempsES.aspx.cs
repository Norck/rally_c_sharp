﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Npgsql;
using System.Globalization;

namespace rally
{
    public partial class tempsES : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                peuplerRally();
                peuplerES();
                peuplerEquipage();
                peuplerFormTemps();
                labelUniqueTemps.Text = dropDownListIdEquipage.SelectedValue;
            }
            
        }

        public void peuplerFormTemps() {
            object[] tabEquipage = new ParticipationES().getEquipageInscritSansTemps(dropDownListES.SelectedValue);
            Panel1.Controls.Clear();
            for (int i = 0; i < tabEquipage.Length; i++)
            {
                Equipage temp = (Equipage)tabEquipage[i];

                TextBox textBox = new TextBox();
                Label label = new Label();

                label.Text = temp.IdEquipage;
                textBox.Visible = true;
                textBox.ID = temp.IdEquipage;
                Panel1.Controls.Add(label);
                Panel1.Controls.Add(textBox);

            }
        }


        public void peuplerRally()
        {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            dropDownListRally.Items.Clear();
            try
            {
                connection = dbConnect.connect();
                object[] tabRally = sqlUtility.multiFind(new Rally(), "rally", null, connection);
                for (int i = 0; i < tabRally.Length; i++)
                {
                    Rally temp = (Rally)tabRally[i];
                    dropDownListRally.Items.Add(new ListItem(temp.NomRally));
                }
            }
            catch (Exception e2)
            {
                throw (e2);
            }
            finally
            {
                connection.Close();
            }
        }

        public void peuplerES()
        {
            EpreuveSpecial ES = new EpreuveSpecial();
            object[] tabES = ES.get(dropDownListRally.SelectedValue);
            dropDownListES.Items.Clear();
            for (int i = 0; i < tabES.Length; i++)
            {
                EpreuveSpecial temp = (EpreuveSpecial)tabES[i];
                dropDownListES.Items.Add(new ListItem(temp.IdES));
            }
        }

        public void Insert()
        {
            TempsES tempsES = new TempsES(dropDownListES.SelectedValue, dropDownListIdEquipage.SelectedValue, Utility.stringToDate(txtUniqueTemps.Text), 1);
            
            SqlUtility sqlUtility = new SqlUtility();
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            NpgsqlTransaction transaction = null;
            try{
                connection = dbConnect.connect();
                transaction = connection.BeginTransaction();
                TempsPassage[] tabTime = new TempsPassage().getTime(txtPath.Text);
                //exception += "ok3";
                TempsPassage[] tabTPass = new TempsPassage[tabTime.Length];
                for (int i = 0; i < tabTime.Length; i++) {
                    tabTPass[i] = new TempsPassage(dropDownListES.SelectedValue, dropDownListIdEquipage.SelectedValue, tabTime[i].Temps, 1, tabTime[i].LatLng);
                    sqlUtility.insertObject(tabTPass[i], "tempsPassage", connection, transaction);
                }
                sqlUtility.insertObject(tempsES, "tempsES", connection, transaction);
                transaction.Commit();


            }catch(Exception e){
                transaction.Rollback();
                message.InnerHtml = "Erreur, cet equipage a déjà un temps.";
                throw (e);
            }
        }
        protected void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                Insert();
            }
            catch (Exception ex) {
                throw (ex);
            }
            
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {

        }

        protected void dropDownListRally_SelectedIndexChanged(object sender, EventArgs e)
        {
            peuplerES();
            peuplerEquipage();
        }

        public void peuplerEquipage()
        {
            dropDownListIdEquipage.Items.Clear();
            object[] tabEquipage = new ParticipationES().getEquipageInscrit(dropDownListES.SelectedValue);
            for (int i = 0; i < tabEquipage.Length; i++)
            {
                Equipage temp = (Equipage)tabEquipage[i];
                dropDownListIdEquipage.Items.Add(new ListItem(temp.IdEquipage));
            }
        }

        protected void btnGetListRally_Click(object sender, EventArgs e)
        {
            

        }

        protected void dropDownListES_SelectedIndexChanged(object sender, EventArgs e)
        {
            peuplerEquipage();
            peuplerFormTemps();
        }

        protected void dropDownListIdEquipage_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelUniqueTemps.Text = dropDownListIdEquipage.SelectedValue;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlUtility sqlUtility = new SqlUtility();
            object[] tabEquipage = new ParticipationES().getEquipageInscrit(dropDownListES.SelectedValue);
            DbConnect dbConnect = new DbConnect();
            NpgsqlConnection connection = null;
            NpgsqlTransaction transaction = null;
            try
            {
                connection = dbConnect.connect();
                transaction = connection.BeginTransaction();
                for (int i = 0; i < tabEquipage.Length; i++)
                {
                    Equipage equipage = (Equipage)tabEquipage[i];
                    if (Request.Form[equipage.IdEquipage] != "") {
                        try
                        {
                            DateTime temps = Utility.stringToDate(Request.Form[equipage.IdEquipage]);
                            TempsES tempsES = new TempsES(dropDownListES.SelectedValue, equipage.IdEquipage, temps, 1);
                            transaction = sqlUtility.insertObject(tempsES, "tempsES", connection, transaction);
                        }
                        catch (Exception ex) {
                            message.InnerHtml = "Un ou plusieurs temps n'ont pas été inserés.";
                        }
                        

                    }
                    
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw (ex);
            }
            finally {
                //transaction.Dispose();
                connection.Close();
            }
            
        }
    }
}