﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Copilote
    {
        private string nomCopilote;
        private string sexe;

        public string NomCopilote
        {
            get
            {
                return nomCopilote;
            }
            set
            {
                nomCopilote = value;
            }
        }

        public string Sexe
        {
            get
            {
                return sexe;
            }
            set
            {
                sexe = value;
            }
        }

        public Copilote() { }

        public Copilote(string nomCopilote, string sexe)
        {
            this.NomCopilote = nomCopilote;
            this.Sexe = sexe;
        }

        public bool insert()
        {
            Boolean succes = false;
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                sqlUtility.insertObject(this, "copilote", connection);
                succes = true;
            }
            catch (Exception e)
            {
                succes = false;
            }
            finally
            {
                connection.Close();
            }

            return succes;
        }
    }

}