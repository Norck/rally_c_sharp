﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Npgsql;
using System.Device.Location;

namespace rally
{
    public partial class epreuveSpecial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            
            Rally rally = new Rally();
            object[] tabRally = rally.getAll();
            if (tabRally != null)
            {
                dropDownListRally.Items.Clear();
                for (int i = 0; i < tabRally.Length; i++)
                {
                    Rally tempRally = (Rally)tabRally[i];
                    dropDownListRally.Items.Add(new ListItem(tempRally.NomRally));
                }
            }
            else {
                errorListRally.InnerHtml = "<p>erreur: Aucun rally n'a été retrouvé.</p>";
            }

            for (int i = 1; i < 15; i++) {
                dropDownListNumES.Items.Add(new ListItem(i.ToString()));
            }


        }

        protected void peuplerListES()
        {
            
            EpreuveSpecial es = new EpreuveSpecial();
            Utility utility = new Utility();
            object[] tabES = es.get(txtNomRallye.Text);
            dropDownListES.Items.Clear();
            for (int i = 0; i < tabES.Length; i++) {
                EpreuveSpecial tempES = (EpreuveSpecial) tabES[i];
                dropDownListES.Items.Add(new ListItem(tempES.IdES));
            }
            
        }

        public void btnGetClassement_Click(object sender, EventArgs e)
        {
            string nomRally = txtNomRallye.Text;
            string es = dropDownListES.SelectedValue;
            HtmlUtility htmlUtility = new HtmlUtility();
            Classement[] classements = new EpreuveSpecial().getClassementES(es);
            htmlTabES.InnerHtml = "<b>Classement " + es + "</b>" + htmlUtility.getHtml(classements, new string[]{"Rang", "VitesseMoyenne", "idEquipage", "Temps", "TempsIdeal", "Distance", "Sexe", "IdCategorieVoiture"});
        }

        protected void btnInsertES_Click(object sender, EventArgs e)
        {

            int numES = Int32.Parse(dropDownListNumES.SelectedValue);
            string nomRally = txtNomRallye.Text;
            string idES = nomRally + "ES" + numES;
            string exception = "";

            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            NpgsqlTransaction transaction = null;
            try
            {
                connection = dbConnect.connect();
                transaction = connection.BeginTransaction();
                object[] tabES = sqlUtility.multiFind(new EpreuveSpecial(), "epreuveSpecial", "idES='" + idES + "'", connection);
                if (tabES.Length != 0)
                {
                    errorNumES.InnerHtml = "<p>erreur: L' ES existe déjà.";
                }
                else {
                    EpreuveSpecial ES = new EpreuveSpecial(idES, dropDownListRally.SelectedValue, new GeoCoordinate(-16.217226, 45.999578),
                        new GeoCoordinate(-16.217226, 45.999578), "ES" + numES.ToString(),
                        (int) float.Parse(distance.Text), Utility.stringToDate(txtTempsIdeal.Text));
                    
                    exception += "ok1";
                    sqlUtility.insertObject(ES, "epreuveSpecial", connection, transaction);
                    
                    exception += "ok2";
                    TrajetES[] tabTrajetES = new EpreuveSpecial().getPath(dropDownListRally.SelectedValue + "ES" + dropDownListNumES.SelectedValue, txtPath.Text);
                    exception += "ok3";
                    for (int i = 0; i < tabTrajetES.Length; i++) {
                        sqlUtility.insertObject(tabTrajetES[i], "trajetES", connection, transaction);
                    }
                    exception += "ok4";
                    transaction.Commit();    
                    message.InnerHtml = "<p>message: Insertion réussite avec succès.</p>";
                }
                

            }
            catch (Exception e2)
            {
                message.InnerHtml = e2.Message;
                transaction.Rollback();
                //throw (e2);
            }
            finally
            {
                transaction.Dispose();
                connection.Close();
            }
        }

        protected void btnGetClassementRally_Click(object sender, EventArgs e)
        {
            string nomRally = txtNomRallye.Text;
            HtmlUtility htmlUtility = new HtmlUtility();
            EpreuveSpecial epreuveSpecial = new EpreuveSpecial();
            string html = "";
            
            Classement[] classements = epreuveSpecial.getClassementRally(nomRally);
            html += "</br><b>Classement " + nomRally + " sans pénalité. </b>" + htmlUtility.getHtml(classements, null);
            //html += "</br><b>Classement " + nomRally + " sans pénalité. </b>" + htmlUtility.getHtml(classements, new String[]{"Point", "Rang", "Penalite", "VitesseMoyenne", "TempsAvecPenalite", "idEquipage", "Temps", "Distance", "Sexe", "IdCategorieVoiture"});
            
            Classement[] penaliteAvec = classements[0].appliquerPenalite(classements);
            html += "</br><b>Classement " + nomRally + " avec pénalité.</b>" + htmlUtility.getHtml(penaliteAvec, null);
            //html += "</br><b>Classement " + nomRally + " avec pénalité.</b>" + htmlUtility.getHtml(penaliteAvec, new String[]{"Point", "Rang", "Penalite", "VitesseMoyenne", "TempsAvecPenalite", "idEquipage", "Temps", "Distance", "Sexe", "IdCategorieVoiture"});
            htmlClassementRally.InnerHtml = html;
        }

        protected void btnGetListES_Click(object sender, EventArgs e)
        {
            peuplerListES();
        }

        protected void bntGetClassementChamp_Click(object sender, EventArgs e)
        {
            Championat championat = new Championat(txtNomChampionat.Text);
            HtmlUtility htmlUtility = new HtmlUtility();
            string html = "";
            try
            {
                html += "<b>Classement " + championat.NomChampionat + " </b>";
                Classement[] tabClassement = championat.getClassement();
                html += htmlUtility.getHtml(tabClassement, new string[] { "rang", "idEquipage", "vitesseMoyenne", "sTemps", "sTempsAvecPenalite", "point" });
            }
            catch (Exception ex) {
                throw (ex);
            }

        }

        protected void txtPath_TextChanged(object sender, EventArgs e)
        {

        }



    }
}