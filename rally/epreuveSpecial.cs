﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Device.Location;

namespace rally
{
    public class EpreuveSpecial
    {
        private string idES;
        private string nomRally;
        private GeoCoordinate geoDepart;
        private GeoCoordinate geoArrivee;
        private string numES;
        private int distance;
        private DateTime tempsIdeal;

        public EpreuveSpecial() { }

        public EpreuveSpecial(string idES, string nomRally, GeoCoordinate geoDepart, GeoCoordinate geoArrivee, string numES, int distance, DateTime tempsIdeal) {
            this.IdES = idES ;
            this.NomRally = nomRally;
            this.GeoDepart = geoDepart;
            this.GeoArrivee = geoArrivee;
            this.NumES = numES;
            this.Distance = distance;
            this.TempsIdeal = tempsIdeal;
        }

        public string IdES{
            get {
                return idES;
            }
            set {
                idES = value;
            }
        }
        public int Distance {
            get {
                return distance;
            }
            set {
                distance = value;
            }
        }
        public DateTime TempsIdeal {
            get {
                return tempsIdeal;
            }
            set {
                tempsIdeal = value;
            }
        }
        public string NomRally {
            get {
                return nomRally;
            }
            set {
                nomRally = value;
            }
        }
        public GeoCoordinate GeoDepart {
            get {
                return geoDepart;
            }
            set {
                geoDepart = value;
            }
        }
        public GeoCoordinate GeoArrivee {
            get {
                return geoArrivee;
            }
            set {
                geoArrivee = value;
            }
        }
        public string NumES {
            get {
                return numES;
            }
            set {
                numES = value;
            }
        }

        public object[] get(string nomRally) {
            object[] result = new object[0];
            DbConnect dbConnect = new DbConnect();
            NpgsqlConnection connection = null ;
            SqlUtility sqlUtil = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                result = sqlUtil.multiFind(this, "epreuveSpecial", "nomRally='" + nomRally + "'", connection);
            }
            catch (Exception e)
            {
                throw(e);
            }
            finally {
                connection.Close();
            }
            return result;
        }

        public object[] getInfoES(string idES) { 
            object[] result = null;
            DbConnect dbConnect = new DbConnect();
            NpgsqlConnection connection = null;
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                InfoES infoES = new InfoES();
                connection = dbConnect.connect();
                result = sqlUtility.multiFind(infoES, "infoES", "idES='" + idES + "'", connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally {
                connection.Close();
            }
            
            return result;
        }

        //mitovy amin'ny getClassement fa ito no tokony ampiasaina amin'ny farany
        public Classement[] getClassementES(string idES)
        {
            object[] infoES = getInfoES(idES);
            Classement[] classements = new Classement[infoES.Length];

            for (int i = 0; i < infoES.Length; i++)
            {

                InfoES tempIES = (InfoES)infoES[i];
                nomRally = tempIES.NomRally;
                string numES = tempIES.NumES;
                string idEquipage = tempIES.IdEquipage;
                DateTime temps = tempIES.Temps;
                DateTime tempsIdeal = tempIES.TempsIdeal;
                int distance = tempIES.Distance;
                string sexe = tempIES.Sexe;
                string idCategorieVoiture = tempIES.IdCategorieVoiture;

                classements[i] = new Classement(nomRally, idES, numES, idEquipage, temps, tempsIdeal, distance, sexe, idCategorieVoiture);

            }
            classements = classements[0].trieTemps2(classements);
            for (int i = 0; i < classements.Length; i++)
            {
                classements[i].Rang = i + 1;
            }

            return classements;
        }

        //public Classement[] getClassementChampionat(String championat) { 
            //List<Classement>
        //}

        public Classement[] getClassementRally(String nomRally) {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            Utility utility = new Utility();
            string exception = "";
            try
            {
                connection = dbConnect.connect();
                object[] dbES = sqlUtility.multiFind(this, "epreuveSpecial", "nomRally='" + nomRally + "'", connection);
                EpreuveSpecial[] tabES = new EpreuveSpecial[dbES.Length];
                for (int i = 0; i < dbES.Length; i++)
                {
                    tabES[i] = (EpreuveSpecial)dbES[i];
                }

                Classement[][] rally = new Classement[tabES.Length][];
                for (int i = 0; i < tabES.Length; i++)
                {
                    try
                    {
                        rally[i] = new Classement[0];
                        rally[i] = getClassementES(tabES[i].IdES);//peut retourner un null s'il n'y a pas encore de classement
                    }
                    catch (Exception e) { 
                        
                    }
                }

                for (int i = 0; i < rally[0].Length; i++)
                {//parcoure les elts du 1er ES
                    DateTime totalTemps = rally[0][i].Temps;
                    int totalDistance = rally[0][i].Distance;
                    float totalSecond = rally[0][i].STemps;
                    for (int u = 1; u < rally.Length; u++)
                    {//parcours les ES a partir du second
                        for (int v = 0; v < rally[u].Length; v++)
                        {
                            if (string.Equals(rally[0][i].IdEquipage, rally[u][v].IdEquipage, StringComparison.OrdinalIgnoreCase))
                            {
                                totalSecond += rally[u][v].STemps;
                                totalTemps = totalTemps.AddSeconds(utility.dateTimeToSeconds(rally[u][v].Temps));
                                totalDistance += rally[u][v].Distance;
                                break;
                            }
                        }
                    }

                    rally[0][i].STemps = totalSecond;
                    rally[0][i].STempsAvecPenalite = totalSecond;

                    //rally[0][i].Temps = totalTemps;
                    rally[0][i].Temps = utility.secondToDate(totalSecond);
                    rally[0][i].TempsAvecPenalite = utility.secondToDate(totalSecond);
                    rally[0][i].Distance = totalDistance;
                }
                exception += "ok3--";
                //tous les infos sur le rally est contenur dans rally[0]

                rally[0] = rally[0][0].trieTemps2(rally[0]);
                for (int i = 0; i < rally[0].Length; i++) {
                    rally[0][i].Rang = i + 1;
                    rally[0][i].calculVitesseMoyenne();
                    rally[0][i].attribuerPoint();
                }

                return rally[0];
            }
            catch (Exception e) {
                throw new Exception (exception + e.Message);
            }
        }
        
        public TrajetES[] getPath(string idES, string data) { 

            string[] tabPoint = data.Split('/');
            TrajetES[] result = new TrajetES[tabPoint.Length];

            for (int i = 0; i < tabPoint.Length; i++) {
                string[] temp = tabPoint[i].Split('|');
                string[] latlng = temp[0].Split(',');
                double lat = double.Parse(latlng[0]);
                double lng = double.Parse(latlng[1]);
                DateTime time = Utility.stringToDate(temp[1]);

                //result[i] = new TrajetES(lat, lng, time);
                result[i] = new TrajetES(idES, new GeoCoordinate(lat, lng));
            }

            return result;

        }
         
    }
}