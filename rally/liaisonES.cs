﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rally
{
    public class LiaisonES
    {
        private string esDepart;
        private string esArrive;
        private DateTime tempsOptimal;

        public string EsDepart {
            get {
                return esDepart;
            }
            set {
                this.esDepart = value;
            }
        }
        public string EsArrive {
            get {
                return esArrive;
            }
            set {
                this.esArrive = esArrive;
            }
        }
        public DateTime TempsOptimal {
            get {
                return tempsOptimal;
            }
            set {
                this.tempsOptimal = value;
            }
        }

        public LiaisonES() { }

        public LiaisonES(string esDepart, string esArrive, DateTime tempsOptimal) {
            this.EsDepart = esDepart;
            this.EsArrive = esArrive;
            this.TempsOptimal = tempsOptimal;
        }
    }
}