﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Rally
    {
        private string nomRally;
        private string nomChampionat;

        public string NomRally {
            get {
                return nomRally;
            }
            set {
                nomRally = value;
            }
        }
        public string NomChampionat {
            get {
                return nomChampionat;
            }
            set {
                this.nomChampionat = value;
            }
        }

        public Rally() { }

        public Rally(string nomRally){
            this.NomRally = nomRally;
            this.NomChampionat = "null";
        }

        public Rally(string nomRally, string nomChampionat) {
            this.NomRally = nomRally;
            this.NomChampionat = nomChampionat;
        }

        public bool insert(){
            Boolean succes = false ;
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                sqlUtility.insertObject(this, "rally", connection);
                succes = true;
            }
            catch (Exception e)
            {
                succes = false;
            }
            finally {
                connection.Close();
            }

            return succes;
        }
        public object[] getAll() {
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            object[] result = null;
            try
            {
                connection = dbConnect.connect();
                result = sqlUtility.multiFind(this, "rally", null, connection);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally {
                connection.Close();
            }
            return result;
        }
    
    }
}