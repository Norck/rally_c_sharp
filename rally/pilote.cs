﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace rally
{
    public class Pilote
    {
        private string nomPilote;
        private string sexe;

        public string NomPilote
        {
            get
            {
                return nomPilote;
            }
            set
            {
                nomPilote = value;
            }
        }

        public string Sexe {
            get {
                return sexe;
            }
            set {
                sexe = value;
            }
        }

        public Pilote() { }

        public Pilote(string nomPilote, string sexe)
        {
            this.NomPilote = nomPilote;
            this.Sexe = sexe;
        }

        public bool insert()
        {
            Boolean succes = false;
            NpgsqlConnection connection = null;
            DbConnect dbConnect = new DbConnect();
            SqlUtility sqlUtility = new SqlUtility();
            try
            {
                connection = dbConnect.connect();
                sqlUtility.insertObject(this, "pilote", connection);
                succes = true;
            }
            catch (Exception e)
            {
                succes = false;
            }
            finally
            {
                connection.Close();
            }

            return succes;
        }
    }

}