﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace rally
{
    public partial class rally : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertRally_Click(object sender, EventArgs e)
        {
            Rally rally = new Rally(txtNomRally.Text);
            bool success = rally.insert();
            if (success)
            {
                message.InnerHtml = "message: Insertion réussite.";
            }
            else {
                message.InnerHtml = "erreur: Ce rally existe déjà.";
            }
        }
    }
}